/**
 * Created by Paulius on 2/18/2017.
 */
import axios from 'axios'

export const loadSuccess = (res) => {
    return {
        type: 'LOADING_CATEGORIES_SUCCESS',
        payload: res
    }
}

export const loadFailed = (error) => {
    return {
        type: 'LOADING_CATEGORIES_FAILED',
        error: error
    }
}

export const loadCategories = () => {
    return dispatch => {
        axios.get('http://localhost:8000/catalogs')
            .then((res) => {
                dispatch(loadSuccess(res.data))
            })
            .catch((error) => {
                dispatch(loadFailed(error))
            })
    }
}

