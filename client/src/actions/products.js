/**
 * Created by Paulius on 2/18/2017.
 */
import axios from 'axios'

export const loadSuccess = (res) => {
    return {
        type: 'LOADING_PRODUCTS_SUCCESS',
        payload: res
    }
}

export const loadFailed = (error) => {
    return {
        type: 'LOADING_PRODUCTS_FAILED',
        error: error
    }
}

export const loadProducts = (id) => {
    return dispatch => {
        axios.get('http://localhost:8000/products/' + id)
            .then((res) => {
                dispatch(loadSuccess(res.data))
            })
            .catch((error) => {
                dispatch(loadFailed(error))
            })
    }
}