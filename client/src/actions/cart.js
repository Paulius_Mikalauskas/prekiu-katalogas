/**
 * Created by Paulius on 2/19/2017.
 */

export const addItem = (item) => {
    return {
        type: 'ADD_CART_ITEM',
        payload: item
    }
}

export const removeItem = (id, amount) => {
    return {
        type: 'REMOVE_CART_ITEM',
        payload: {
            id: id,
            amount: amount
        }
    }
}

export const incrementAmount = (id) => {
    return {
        type: 'INCREMENT_AMOUNT',
        payload: {
            id: id,
        }
    }
}

export const decrementAmount = (id) => {
    return {
        type: 'DECREMENT_AMOUNT',
        payload: {
            id: id,
        }
    }
}

export const removeAllItems = () => {
    return {
        type: 'REMOVE_ALL_ITEMS',
    }
}