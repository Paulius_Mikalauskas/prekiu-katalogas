/**
 * Created by Paulius on 2/18/2017.
 */
export const Categories = () => {
    return [
        {
            id: 1,
            category: 'Monitoriai',
            img: 'https://www.sisense.com/wp-content/uploads/Sisense-real-analytics-accurate-insights-242x200.png'
        },
        {
            id:2,
            category: 'Telefonai',
            img: 'http://www.zath.co.uk/wp-content/uploads/2011/02/Article-Search-242x2681-242x200.jpg'
        }
    ]
}

export const Products = () => {
    return [
        {
            id: 1,
            categoryId: 1,
            name: 'BenQ GW2270 LED',
            img: 'http://www.skytech.lt/images/xsmall/52/477352.jpg',
            price: '97.59'
        },
        {
            id: 2,
            categoryId: 1,
            name: 'BenQ GL2460 LED',
            img: 'http://www.skytech.lt/images/xsmall/9/337109.jpg',
            price: '119.49'
        }
    ]
}