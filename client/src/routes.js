/**
 * Created by Paulius on 2/18/2017.
 */
import React from 'react';
import { Route, Router, browserHistory, IndexRoute } from 'react-router';
import MainContainer from './containers/MainContainer'
import Home from './containers/Home'
import Category from './containers/Category'
import ShoppingCart from './containers/ShoppingCart'
import './index.css'


export default (
    <Router history={browserHistory}>
        <Route path='/' component={MainContainer}>
            <IndexRoute component={Home}/>
            <Route path="/category/:id" component={Category} />
            <Route path="/cart" component={ShoppingCart} />
        </Route>
    </Router>
)