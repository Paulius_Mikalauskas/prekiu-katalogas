/**
 * Created by Paulius on 2/18/2017.
 */
import { applyMiddleware, createStore} from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk';
import reducer from './reducers/index'
import { loadState } from './middlewares/localstorage'

const persistedState = loadState()

const middleware = applyMiddleware(thunk, logger())

export default createStore(reducer, middleware)