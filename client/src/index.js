import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux'
import './index.css';
import store from './store'
import { saveState} from './middlewares/localstorage'

store.subscribe(() => {
    saveState(store.getState())
})

const Index = () => {
    return(
        <Provider store={store}>
            <App />
        </Provider>
    )
}

ReactDOM.render(
 <Index />,
  document.getElementById('root')
);
