/**
 * Created by Paulius on 2/18/2017.
 */
const initialState = {
    categories: []
}

const categories = (state = initialState, action) => {
    switch (action.type) {
        case 'LOADING_CATEGORIES_SUCCESS':
            return {...state, categories: action.payload}
        default:
            return state
    }
}

export default categories