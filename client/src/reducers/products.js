/**
 * Created by Paulius on 2/18/2017.
 */
const initialState = {
    products: []
}

const products = (state = initialState, action) => {
    switch (action.type) {
        case 'LOADING_PRODUCTS_SUCCESS':
            return {...state, products: action.payload}
        default:
            return state
    }
}

export default products