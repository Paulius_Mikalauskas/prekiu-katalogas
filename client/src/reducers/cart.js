/**
 * Created by Paulius on 2/19/2017.
 */
const initialState = {
    cartItems: []
}

const itemExists = (state, id) => {
    var exists = false;
    state.cartItems.forEach((item) => {
        if(item.id === id) exists = true;
    })
    return exists;
}

const addItem = (state, action) => {
    return {...state, cartItems: [...state.cartItems, {...action.payload, amount: 1} ]}
}

const cart = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_CART_ITEM':
            if(state.cartItems.length === 0) {
                addItem(state, action)
            }
            if(itemExists(state, action.payload.id)) {
                return {...state, cartItems: state.cartItems.map((item) => {
                    return item.id === action.payload.id ? {...item, amount: item.amount + 1} : item
                })}
            } else {
                return addItem(state, action)
            }
        case 'REMOVE_CART_ITEM':
            var items = state.cartItems.filter((item) => {
                return item.id != action.payload.id
            })
            return {...state, cartItems: items}
        case 'INCREMENT_AMOUNT':
            return {...state, cartItems: state.cartItems.map((item) => {
                if(item.id === action.payload.id) {
                    return {...item, amount: item.amount + 1}
                } else return item
            })}
        case 'DECREMENT_AMOUNT':
            return {...state, cartItems: state.cartItems.map((item) => {
                if(item.id === action.payload.id) {
                    return item.amount === 1? item : {...item, amount: item.amount - 1}
                } else return item
            })}
        case 'REMOVE_ALL_ITEMS':
            return {...state, cartItems: []}
        default:
            return state
    }
}

export default cart