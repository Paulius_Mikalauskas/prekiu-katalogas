/**
 * Created by Paulius on 2/18/2017.
 */
import React from 'react'
import { Navbar, NavItem, Nav, MenuItem, NavDropdown } from 'react-bootstrap'
import { connect } from 'react-redux';
import { Link } from 'react-router'

const cartBtn = {
    cursor: 'pointer'
}

class MainContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    getCorrectName(amount) {
        if(amount == 1) {
            return 'prekė'
        } else if (amount < 10) {
            return 'prekės'
        } else {
            return 'prekių'
        }
    }

    getCartItems() {
        var amount = 0
        this.props.cartItems.map((item) => {
            amount = amount + item.amount
        })
        return amount
    }

    render() {
        return(
            <div>
                <div>
                    <Navbar>
                        <Navbar.Header>
                            <Navbar.Brand>
                                <span>E-shop</span>
                            </Navbar.Brand>
                        </Navbar.Header>
                        <Navbar.Text style={cartBtn}>
                            <Link to={'/'}>
                                Home
                            </Link>
                        </Navbar.Text>
                        <Navbar.Text pullRight style={cartBtn}>
                            <Link to={'/cart'}>
                            {
                                this.getCartItems() > 0 ?
                                    'Krepšelyje yra ' + this.getCartItems() + ' ' + this.getCorrectName(this.getCartItems()):
                                    'Prekių krepšelis tusčias'
                            }
                            </Link>
                        </Navbar.Text>
                    </Navbar>
                </div>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cart.cartItems
    };
}


export default connect(mapStateToProps)(MainContainer)