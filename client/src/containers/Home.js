/**
 * Created by Paulius on 2/18/2017.
 */
import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { loadCategories } from '../actions/categories'
import { Categories } from '../mock-data'
import {Grid, Row, Col, Button, Thumbnail} from 'react-bootstrap'
import { Link } from 'react-router'

const style = {
    padding: 0,
    paddingRight: '15px'
}

const thumbStyle = {
    cursor: 'pointer'
}

class Home extends React.Component {

    componentDidMount() {
        this.props.loadCategories()
    }

    render() {
        const categories = this.props.categories.categories.map((item) => {
            return(
                <Col style={style} xs={6} md={4} key={item.id}>
                    <Link to={`/category/${item.id}`}>
                    <Thumbnail style={thumbStyle} src={item.img} alt="242x200">
                        <h3>{item.category}</h3>
                    </Thumbnail>
                    </Link>
                </Col>
            )
        })
        return(
            <Grid>
                <Row>
                    {categories}
                </Row>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.categories
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        loadCategories: loadCategories
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)