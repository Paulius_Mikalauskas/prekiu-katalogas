/**
 * Created by Paulius on 2/18/2017.
 */
import React from 'react'
import {Media, Button, Grid, Row, Col, FormGroup, FormControl} from 'react-bootstrap'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { removeItem, incrementAmount, decrementAmount, removeAllItems } from '../actions/cart'

const inputStyle = {
    display: 'inline-block',
    width: '60px',
    paddingLeft: '10px',
    paddingRight: '10px',
    textAlign: 'center'
}

const mediaStyle = {
    border: '1px solid #E0E0E0',
    padding: '10px',
    borderRadius: '5px'
}

const incrementButtons = {
    display: 'inline-block',
}

class ShoppingCart extends React.Component {

    getAllPrice() {
        var price = 0
        this.props.cartItems.forEach((item) => {
            price = price + item.amount * parseFloat(item.price)
        })
        return price.toFixed(2)
    }

    render() {
        console.log(this.props.cartItems)
        var items = this.props.cartItems.map((item) => {
            return(
            <Col xs={12} md={12} key={item.id}>
                <Media>
                    <Media.Left>
                        <img width={64} height={64} src={item.img} alt="item"/>
                    </Media.Left>
                    <Media.Body>
                        <Media.Heading>{item.name}</Media.Heading>
                        <div>Kaina {item.price}€, Kiekis
                            <FormGroup style={inputStyle} bsSize="small">
                                <FormControl readOnly={true} value={item.amount} type="text"/>
                            </FormGroup>
                            <Button onClick={() => this.props.incrementAmount(item.id)} style={incrementButtons} bsStyle="default">+</Button>
                            <Button onClick={() => this.props.decrementAmount(item.id)} style={incrementButtons} bsStyle="default">-</Button>
                        </div>
                        <div>Iš viso {(item.amount * parseFloat(item.price)).toFixed(2) + '€'}</div>
                    </Media.Body>
                    <Media.Right>
                        <Button onClick={() => this.props.removeItem(item.id, item.amount)} bsStyle="danger">Išimti</Button>
                    </Media.Right>
                    <hr/>
                </Media>
            </Col>
            )
        })
        return(
            <Grid>
                <Row style={mediaStyle}>
                    {items}
                    <h3>Galutinė suma {this.getAllPrice()}€</h3>
                    <Button style={{float: 'right'}} bsStyle="success">Išsaugoti krepšelį</Button>
                    <Button style={{float: 'right', marginRight: '10px'}} onClick={() => this.props.removeAllItems()} bsStyle="danger">Išvalyti krepšelį</Button>
                </Row>

            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cart.cartItems
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        removeItem: removeItem,
        incrementAmount: incrementAmount,
        decrementAmount: decrementAmount,
        removeAllItems: removeAllItems
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart)
