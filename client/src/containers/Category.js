/**
 * Created by Paulius on 2/18/2017.
 */
import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { Products } from '../mock-data'
import { loadProducts } from '../actions/products'
import { addItem } from '../actions/cart'
import {Grid, Row, Col, Button, Thumbnail} from 'react-bootstrap'

const style = {
    padding: 0,
    paddingRight: '15px'
}

const cartBtn = {
    float: 'right'
}

class Category extends React.Component {

    componentDidMount() {
        this.props.loadProducts(this.props.params.id)
    }

    addItem(id) {
        this.props.addItem(id)
    }

    render() {
        const products = this.props.products.products.map((item) => {
            if(item.categoryId != this.props.params.id) return undefined;
            return(
                <Col style={style} xs={6} md={4} key={item.id}>
                        <Thumbnail src={item.img} alt="242x200">
                            <h3>{item.name}</h3>
                            <div style={{height: '40px'}}>
                                <Button onClick={() => this.addItem(item)} style={cartBtn} bsStyle="primary">į krepšelį</Button>
                            </div>
                        </Thumbnail>
                </Col>
            )
        })
        return(
            <Grid>
                <Row>
                    {products}
                </Row>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        loadProducts: loadProducts,
        addItem: addItem
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)