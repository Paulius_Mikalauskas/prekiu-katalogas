<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller {

    public function getProducts(Request $request) {

        return DB::table('products')->where('categoryId', $request->id)->get();
    }
}