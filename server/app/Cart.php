<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';

    public function cart() {
      return $this->hasMany('App\CartItem');
    }
}
