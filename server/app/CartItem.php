<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'cart_id', 'created_at', 'updated_at'];
    protected $table = 'cart_items';
}
