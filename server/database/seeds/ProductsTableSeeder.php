<?php

use Illuminate\Database\Seeder;


class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('products')->insert([
            [
                'id' => 1,
                'categoryId' => 1,
                'price' => '83.29',
                'name' => 'Acer V6 V206HQLBb LED',
                'img' => 'http://www.skytech.lt/images/medium/24/629724.jpg'
            ],
            [
                'id' => 2,
                'categoryId' => 1,
                'price' => '136.89',
                'name' => 'Acer K2 K242HLAbid LED',
                'img' => 'http://www.skytech.lt/images/medium/55/629755.jpg'
            ],
            [
                'id' => 3,
                'categoryId' => 1,
                'price' => '76.89',
                'name' => 'BenQ GL955A LED',
                'img' => 'http://www.skytech.lt/images/medium/58/332158.jpg'
            ],
            [
                'id' => 4,
                'categoryId' => 1,
                'price' => '142.19',
                'name' => 'BenQ GL2450HM LED',
                'img' => 'http://www.zath.co.uk/wp-content/uploads/2010/11/785ba8ca7f40a0f8beb7179f20341bddc7a_1Screenshot-242x2051-242x200.jpg'
            ],
            [
                'id' => 5,
                'categoryId' => 1,
                'price' => '22.15',
                'name' => 'BenQ',
                'img' => 'http://www.zath.co.uk/wp-content/uploads/2010/11/785ba8ca7f40a0f8beb7179f20341bddc7a_1Screenshot-242x2051-242x200.jpg'
            ],

            // Mobilieji

            [
                'id' => 10,
                'categoryId' => 2,
                'price' => '404.99',
                'name' => 'Samsung Galaxy S6 G920F 32GB',
                'img' => 'https://www.varle.lt/static/uploads/products/17/pho/phone-g920f-galaxy-s6-flat-32gb-black.jpeg'
            ],
            [
                'id' => 11,
                'categoryId' => 2,
                'price' => '269.00',
                'name' => 'Elephone P9000 4GB/32GB DS',
                'img' => 'https://www.varle.lt/static/uploads/products/144/tel/tel-elephone-p9000-32gb-ds-baltas.jpg'
            ],
            [
                'id' => 12,
                'categoryId' => 2,
                'price' => '269.00',
                'name' => 'Elephone P9000 4GB/32GB DS',
                'img' => 'https://www.varle.lt/static/uploads/products/144/tel/tel-elephone-p9000-32gb-ds-baltas.jpg'
            ],
            [
                'id' => 13,
                'categoryId' => 2,
                'price' => '269.00',
                'name' => 'Elephone P9000 4GB/32GB DS',
                'img' => 'https://www.varle.lt/static/uploads/products/144/tel/tel-elephone-p9000-32gb-ds-baltas.jpg'
            ],
            [
                'id' => 14,
                'categoryId' => 2,
                'price' => '269.00',
                'name' => 'Elephone P9000 4GB/32GB DS',
                'img' => 'https://www.varle.lt/static/uploads/products/144/tel/tel-elephone-p9000-32gb-ds-baltas.jpg'
            ],

         ]);
    }
}
