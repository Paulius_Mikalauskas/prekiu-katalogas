<?php

use Illuminate\Database\Seeder;

class CatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('catalogs')->insert([
            [
                "id" => 1,
                "category" => "Monitoriai",
                "img" => "https://www.sisense.com/wp-content/uploads/Sisense-real-analytics-accurate-insights-242x200.png"
            ],
            [
                "id" => 2,
                "category" => "Telefonai",
                "img" => "http://www.zath.co.uk/wp-content/uploads/2011/02/Article-Search-242x2681-242x200.jpg"
            ],
        ]);
    }
}
